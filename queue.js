let collection = [];

function enqueue(item) {
    collection = [...collection, item]
    return collection
}

function dequeue() {
    let item;
    [item, ...collection] = collection;
    return collection;
}

function front() {
    if(isEmpty() === false) {
        return collection[0];
    }
}

function size() {
    return collection.length;
}

function isEmpty() {
    return collection.length === 0;
}

function print() {
     return collection;
}


module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};